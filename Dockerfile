FROM centos:7
COPY maxiv.repo /etc/yum.repos.d/
COPY pylon-5.0.12.11829-x86_64.tar.gz .
RUN yum install -y epel-release && yum makecache
RUN tar -zxvf pylon-5.0.12.11829-x86_64.tar.gz && cd pylon-5.0.12.11829-x86_64 && tar -C /opt -xzf pylonSDK*.tar.gz && cd ..
RUN yum install -y \
    python-pytango \
    python-pip \
    python-devel \
    git \
    gcc \
    gcc-devel \
    gcc-c++ \
    swig3 \
    && yum clean all
RUN git clone https://github.com/basler/pypylon.git && cd pypylon && pip install . && cd ..
COPY Basler/Basler.py /
ENTRYPOINT ["python", "/Basler.py"]
