# Basler camera

## Installation

### Prerequirements

Before installing pylon and pypylon please install all needed packages by executing:

```
sudo apt install tango-common libboost-dev libboost-all-dev libtango-dev swig
pip3 install PyTango
```

Download the latest pypylon package from: https://www.baslerweb.com/en/products/software/basler-pylon-camera-software-suite/

install it using package manager:

```
sudo dpkg -i pylon_5.0.12.11829-deb0_arm64.deb
```

pypylon installation

```
git clone https://github.com/basler/pypylon.git
cd pypylon
pip install .
```

### Basler camera DS installation

```
git clone https://gitlab.com/s2innovation-partners/icfo/baslerds.git
cd baslerds
sudo python3 setup.py install
```