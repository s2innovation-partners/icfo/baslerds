import random

from tango.server import Device, attribute, device_property, DeviceMeta, command, run
from tango import DebugIt, DispLevel, AttrWriteType, DevState
from pypylon import pylon


class Basler(Device):
    __metaclass__ = DeviceMeta

    # ---------------------------------
    #   Properties
    # ---------------------------------

    IP = device_property(
        dtype=str,
        mandatory=True,
        doc="IP of Basler camera"
    )

    ReadPeriod = device_property(
        dtype=int,
        default_value=1000,
        doc="Polling period for reading data in ms for Basler camera"
    )

    Width = device_property(
        dtype=int,
        mandatory=True,
        doc="Width of Basler's image"
    )

    Height = device_property(
        dtype=int,
        mandatory=True,
        doc="Height of Basler's image"
    )

    FrameRate = device_property(
        dtype=int,
        mandatory=True,
        doc="Frame rate of Basler camera"
    )

    Simulated = device_property(
        dtype=bool,
        default_value=False,
        doc="""Data generation mode. If True data are randomly generated.
         If False data is obtained from Basler camera"""
    )

    # ---------------------------------
    #   Global methods
    # ---------------------------------

    def init_device(self):
        Device.init_device(self)
        self.set_state(DevState.INIT)
        self._image = [[0 for x in range(self.Width)] for y in range(self.Height)]
        self._exposure_time = 0
        self._x = 0
        self._y = 0
        self._radius = 0
        self._connected = False
        self.set_state(DevState.ON)
        if not self.Simulated:
            try:
                self.connect()
            except Exception as basic_exception:  # pylint: disable=broad-except
                self.error_stream(str(basic_exception))
                self.set_state(DevState.FAULT)
                self.set_status("Exception during establishing connection.")
                try:
                    self.camera.Close()
                except:
                    self.set_status("Exception during closing connection.")

    def always_executed_hook(self):
        pass

    def delete_device(self):
        self.disconnect()

    # ---------------------------------
    #   Attributes
    # ---------------------------------

    exposure_time = attribute(label="Exposure time",
                              dtype=int,
                              polling_period=ReadPeriod.default_value,
                              display_level=DispLevel.OPERATOR,
                              access=AttrWriteType.READ_WRITE,
                              unit="us",
                              fget="get_exposure_time",
                              fset="set_exposure_time",
                              doc="Exposure time")

    x_position = attribute(label="X position",
                           dtype=float,
                           polling_period=ReadPeriod.default_value,
                           display_level=DispLevel.OPERATOR,
                           access=AttrWriteType.READ_WRITE,
                           fget="get_x_position",
                           fset="set_x_position",
                           doc="The X position of the circle")

    y_position = attribute(label="Y position",
                           dtype=float,
                           polling_period=ReadPeriod.default_value,
                           display_level=DispLevel.OPERATOR,
                           access=AttrWriteType.READ_WRITE,
                           fget="get_y_position",
                           fset="set_y_position",
                           doc="The Y position of the circle")

    radius = attribute(label="Radius",
                       dtype=float,
                       polling_period=ReadPeriod.default_value,
                       display_level=DispLevel.OPERATOR,
                       access=AttrWriteType.READ_WRITE,
                       fget="get_radius",
                       fset="set_radius",
                       doc="The radius of the circle")

    image = attribute(label="Image",
                      dtype=((float,),),
                      polling_period=ReadPeriod.default_value,
                      display_level=DispLevel.OPERATOR,
                      access=AttrWriteType.READ,
                      max_dim_x=1920,
                      max_dim_y=1200,
                      fget="get_image",
                      doc="Image from the Basler camera.")

    def get_exposure_time(self):
        if self.Simulated:
            return int(random.uniform(19, 10000000))
        if self._connected:
            self._exposure_time = int(self.camera.ExposureTime.GetValue())
        return self._exposure_time

    def get_image(self):
        if self.Simulated:
            return [
                [
                    int(random.uniform(0, 255)) for x in range(self.Width)
                ] for y in range(self.Height)
            ]
        if self._connected:
            try:
                self.camera.StartGrabbingMax(1)
                grab_result = self.camera.RetrieveResult(3000,
pylon.TimeoutHandling_ThrowException)
            except:
                self.connect()
                return self._image

            if grab_result.GrabSucceeded():
                self._image = grab_result.Array
                grab_result.Release()
            else:
                self.error_stream("Error: %s %s" %
                                  str(grab_result.ErrorCode),
                                  str(grab_result.ErrorDescription))
                self.disconnect()
                self.connect()

        return self._image

    def get_x_position(self):
        if self.Simulated:
            return int(random.uniform(0, 1920))
        return self._x

    def get_y_position(self):
        if self.Simulated:
            return int(random.uniform(0, 1200))
        return self._y

    def get_radius(self):
        if self.Simulated:
            return int(random.uniform(0, 500))
        return self._radius

    def set_exposure_time(self, value):
        if self.Simulated:
            self._exposure_time = int(value)
        else:
            self.camera.ExposureTime.SetValue(int(value))

    def set_x_position(self, value):
        self._x = int(value)

    def set_y_position(self, value):
        self._y = int(value)

    def set_radius(self, value):
        self._radius = int(value)

    # ---------------------------------
    #   Commands
    # ---------------------------------

    @command
    def connect(self):
        try:
            factory = pylon.TlFactory.GetInstance()
            ptl = factory.CreateTl('BaslerGigE')
            empty_camera_info = ptl.CreateDeviceInfo()
            empty_camera_info.SetPropertyValue('IpAddress', self.IP)
            camera_device = factory.CreateDevice(empty_camera_info)
            self.camera = pylon.InstantCamera(camera_device)

            try:
                self.camera.Open()
            except:
                self.disconnect()
                self.connect()

            try:
                if self.camera.Width.Value != self.Width:
                    self.camera.Width = self.Width
                if self.camera.Height.Value != self.Height:
                    self.camera.Height = self.Height

            except:
                self.disconnect()
                self.connect()


            self.camera.AcquisitionFrameRateEnable = True
            self.camera.AcquisitionFrameRate = self.FrameRate
            self._connected = True
            self.set_state(DevState.ON)
            self.set_status("The camera is connected.")
        except:
            self.set_state(DevState.FAULT)
            self.set_status('Camera with IP: {} not found'.format(self.IP))

    @command
    def disconnect(self):
        self.camera.Close()
        self._connected = False
        self.set_state(DevState.OFF)
        self.set_status("The camera is disconnected.")


def main(args=None, **kwargs):
    return run((Basler,), args=args, **kwargs)


if __name__ == '__main__':
    main()
