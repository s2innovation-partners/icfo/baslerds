import setuptools

setuptools.setup(
    name="tangods-basler",
    version="1.0.0",
    description="Device server for Basler camera",
    author="S2innovation",
    author_email="contact@s2innovation.com",
    license="Proprietary",
    classifiers=[
        'License :: Other/Proprietary License'],
    packages=setuptools.find_packages(),
    entry_points={
        'console_scripts': [
            "Basler=Basler.Basler:main",
        ]
    }

)

